import { Component, OnInit } from '@angular/core';
import { Hero } from '../../model/hero';
import { HeroService } from '../../services/hero-service/hero-service.service';

@Component({
  selector: 'ai4tutorial-app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss'],
})
export class HeroesComponent implements OnInit {
  heroes: Hero[] = [];
  selectedHeroId?: number;
  newHeroName = '';

  constructor(private heroService: HeroService) {}

  ngOnInit(): void {
    this.heroService.getHeroes().subscribe((heroes) => (this.heroes = heroes));
  }

  onSelect(hero: Hero): void {
    this.selectedHeroId = hero.id;
  }

  deleteHero(id: number) {
    if (this.selectedHeroId === id) this.selectedHeroId = undefined;
    this.heroService.deleteHeroById(id);
  }

  getHeroById(id: number | undefined) {
    return id ? this.heroService.getHeroById(id) : undefined;
  }

  addHero() {
    if (!this.newHeroName) return;
    this.heroService.addHero(this.newHeroName);
    this.newHeroName = '';
  }
}
