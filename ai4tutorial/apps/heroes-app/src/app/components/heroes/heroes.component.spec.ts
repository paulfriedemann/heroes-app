import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeroDetailComponent } from '../hero-detail/hero-detail.component';

import { HeroesComponent } from './heroes.component';

describe('HeroesComponent', () => {
  let component: HeroesComponent;
  let fixture: ComponentFixture<HeroesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeroesComponent, HeroDetailComponent],
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MatListModule,
        MatCardModule,
        FormsModule,
        MatInputModule,
        MatIconModule,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should select item after click', () => {
    const compiled = fixture.nativeElement as HTMLElement;
    const firstSelectableElement = compiled.querySelector('mat-list-option');
    expect(firstSelectableElement).toBeTruthy();
    firstSelectableElement?.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    expect(firstSelectableElement?.className).toContain('selected');
  });

  it('should display selected item in details', () => {
    const compiled = fixture.nativeElement as HTMLElement;
    const firstSelectableElement = compiled.querySelector('mat-list-option');
    expect(firstSelectableElement).toBeTruthy();
    firstSelectableElement?.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      const selectedHero = component.getHeroById(component.selectedHeroId);
      expect(firstSelectableElement?.textContent).toContain(selectedHero?.name);
      const detailViewInput = compiled.querySelector('input');
      expect(detailViewInput?.value).toBe(selectedHero?.name);
    });
  });
});
