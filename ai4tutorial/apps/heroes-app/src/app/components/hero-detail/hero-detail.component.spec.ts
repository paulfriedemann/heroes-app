import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HEROES } from '../../services/mock-heroes';

import { HeroDetailComponent } from './hero-detail.component';

describe('HeroDetailComponent', () => {
  let component: HeroDetailComponent;
  let fixture: ComponentFixture<HeroDetailComponent>;
  const testHero = HEROES[0];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeroDetailComponent],
      imports: [
        BrowserModule,
        FormsModule,
        BrowserAnimationsModule,
        MatCardModule,
        MatInputModule,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display name and id correctly', () => {
    component.hero = testHero;
    const compiled = fixture.nativeElement;
    fixture.detectChanges();
    expect(compiled.querySelector('mat-card-title')?.textContent).toContain(
      testHero.name.toUpperCase()
    );
    expect(compiled.querySelector('mat-card-subtitle')?.textContent).toContain(
      testHero.id.toString()
    );
  });
  it('should display name in input', () => {
    component.hero = testHero;
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      const compiled = fixture.nativeElement;
      const input = compiled.querySelector('input');
      expect(input?.value).toContain(testHero.name);
    });
  });

  it('should react to input events', () => {
    component.hero = testHero;
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    const input = compiled.querySelector('input');
    if (!input) {
      expect(input).toBeTruthy();
      return;
    }
    input.value = 'Test Hero';
    input?.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component?.hero?.name).toBe('Test Hero');
    });
  });

  it('should not find input without selected hero', () => {
    const compiled = fixture.nativeElement;
    const input = compiled.querySelector('input');
    expect(input).toBeFalsy();
  });
});
