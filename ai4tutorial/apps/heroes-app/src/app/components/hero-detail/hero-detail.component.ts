import { Component, OnInit, Input } from '@angular/core';
import { Hero } from '../../model/hero';
import { HeroService } from '../../services/hero-service/hero-service.service';

@Component({
  selector: 'ai4tutorial-app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.scss'],
})
export class HeroDetailComponent implements OnInit {
  @Input() hero?: Hero;

  constructor(private heroService: HeroService) {}

  ngOnInit(): void {}

  updateHero(id: number, name: string) {
    this.heroService.updateHero(id, name);
  }
}
