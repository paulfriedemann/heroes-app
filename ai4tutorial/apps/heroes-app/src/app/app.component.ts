import { Component } from '@angular/core';

@Component({
  selector: 'ai4tutorial-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'Heroes';
}
