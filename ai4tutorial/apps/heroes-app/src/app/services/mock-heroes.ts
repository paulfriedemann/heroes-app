import { Hero } from '../model/hero';

export const HEROES: Hero[] = [
  { id: 11, name: 'Cool Hero' },
  { id: 12, name: 'Lazy Hero' },
  { id: 13, name: 'Clumsy Hero' },
  { id: 14, name: 'Tiny Hero' },
  { id: 15, name: 'Odd Hero' },
  { id: 16, name: 'Friendly Hero' },
  { id: 17, name: 'Honest Hero' },
];
