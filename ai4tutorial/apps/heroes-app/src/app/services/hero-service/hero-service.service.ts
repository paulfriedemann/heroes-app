import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Hero } from '../../model/hero';
import { HEROES } from '../mock-heroes';
@Injectable({
  providedIn: 'root',
})
export class HeroService {
  private heroes: BehaviorSubject<Hero[]>;

  constructor() {
    this.heroes = new BehaviorSubject([] as Hero[]);
    this.heroes.next(HEROES);
  }
  getHeroes(): Observable<Hero[]> {
    return this.heroes;
  }

  getHeroByName(name: string): Hero | undefined {
    return this.heroes.getValue().find((h) => h.name === name);
  }

  getHeroById(id: number): Hero | undefined {
    return this.heroes.getValue().find((h) => h.id === id);
  }
  addHero(name: string) {
    const currentHeroes = this.heroes.getValue();
    const newId = Math.max(...currentHeroes.map((h) => h.id)) + 1;
    this.heroes.next([...currentHeroes, { id: newId, name }]);
  }
  updateHero(id: number, name: string) {
    this.heroes.next(
      this.heroes.getValue().map((h) => (h.id === id ? { ...h, name } : h))
    );
  }
  deleteHeroById(id: number) {
    this.heroes.next(this.heroes.getValue().filter((h) => h.id !== id));
  }
}
