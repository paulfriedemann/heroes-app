import { TestBed } from '@angular/core/testing';
import { HEROES } from '../mock-heroes';
import { HeroService } from './hero-service.service';

describe('HeroService', () => {
  let service: HeroService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HeroService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return correct hero by id', () => {
    const testHero = HEROES[0];
    const hero = service.getHeroById(testHero.id);
    expect(hero?.id).toBe(testHero.id);
  });

  it('should return correct hero by name', () => {
    const testHero = HEROES[0];
    const hero = service.getHeroByName(testHero.name);
    expect(hero?.id).toBe(testHero.id);
  });

  it('should rename hero correctly', () => {
    const testHero = HEROES[0];
    const testName = 'Test Hero';
    service.updateHero(testHero.id, testName);
    expect(service.getHeroById(testHero.id)?.name).toBe(testName);
  });

  it('should delete the hero correctly', () => {
    const testHero = HEROES[0];
    service.deleteHeroById(testHero.id);
    expect(service.getHeroById(testHero.id)).toBeUndefined();
  });

  it('should create new hero', () => {
    const newName = 'Test Hero';
    service.addHero(newName);
    expect(service.getHeroByName(newName)?.name).toBe(newName);
  });
});
